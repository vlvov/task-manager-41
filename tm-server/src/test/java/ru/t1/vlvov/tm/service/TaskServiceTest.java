package ru.t1.vlvov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.vlvov.tm.api.service.IConnectionService;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.api.service.ITaskService;
import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.marker.UnitCategory;
import ru.t1.vlvov.tm.dto.model.TaskDTO;

import static ru.t1.vlvov.tm.constant.TaskTestData.*;
import static ru.t1.vlvov.tm.constant.UserTestData.USER1;
import static ru.t1.vlvov.tm.constant.UserTestData.USER2;

@Category(UnitCategory.class)
public final class TaskServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @After
    public void tearDown() {
        taskService.clear();
    }

    @Test
    public void add() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        taskService.add(USER1_TASK1);
        Assert.assertEquals(USER1_TASK1.getId(), taskService.findAll().get(0).getId());
    }

    @Test
    public void addByUserId() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        taskService.add(USER1.getId(), USER1_TASK1);
        Assert.assertEquals(USER1_TASK1.getId(), taskService.findAll().get(0).getId());
        Assert.assertEquals(USER1.getId(), taskService.findAll().get(0).getUserId());
    }

    @Test
    public void clearByUserId() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        taskService.add(USER1_TASK_LIST);
        Assert.assertFalse(taskService.findAll(USER1.getId()).isEmpty());
        taskService.clear(USER2.getId());
        Assert.assertFalse(taskService.findAll(USER1.getId()).isEmpty());
        taskService.clear(USER1.getId());
        Assert.assertTrue(taskService.findAll().isEmpty());
        taskService.add(USER2_TASK1);
        taskService.clear(USER1.getId());
        Assert.assertFalse(taskService.findAll().isEmpty());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        taskService.add(TASK_LIST);
        Assert.assertFalse(taskService.findAll(USER1.getId()).isEmpty());
    }

    @Test
    public void findOneByIdByUserId() {
        taskService.add(USER1_TASK1);
        taskService.add(USER2_TASK1);
        Assert.assertEquals(USER1_TASK1.getId(), taskService.findOneById(USER1.getId(), USER1_TASK1.getId()).getId());
        Assert.assertNull(taskService.findOneById(USER1.getId(), USER2_TASK1.getId()));
    }

    @Test
    public void removeByUserId() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        taskService.add(USER1_TASK1);
        taskService.add(USER2_TASK1);
        Assert.assertEquals(USER1_TASK1, taskService.remove(USER1.getId(), USER1_TASK1));
        Assert.assertFalse(taskService.findAll().contains(USER1_TASK1));
        Assert.assertNotNull(taskService.findOneById(USER2_TASK1.getId()));
    }

    @Test
    public void removeByIdByUserId() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        taskService.add(USER1_TASK1);
        taskService.add(USER2_TASK1);
        taskService.removeById(USER1.getId(), USER1_TASK1.getId());
        Assert.assertNull(taskService.findOneById(USER1_TASK1.getId()));
        Assert.assertEquals(USER2_TASK1.getId(), taskService.findOneById(USER2_TASK1.getId()).getId());
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        taskService.add(USER1_TASK1);
        Assert.assertTrue(taskService.existsById(USER1_TASK1.getId()));
        Assert.assertFalse(taskService.existsById(USER2_TASK1.getId()));
    }

    @Test
    public void changeTaskStatusById() {
        taskService.add(USER1_TASK1);
        Assert.assertEquals(Status.NOT_STARTED, USER1_TASK1.getStatus());
        taskService.changeTaskStatusById(USER1_TASK1.getUserId(), USER1_TASK1.getId(), Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, taskService.findOneById(USER1_TASK1.getId()).getStatus());
    }

    @Test
    public void createTaskName() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        @NotNull TaskDTO task = taskService.create(USER1.getId(), "test_task");
        Assert.assertEquals(task.getId(), taskService.findOneById(task.getId()).getId());
        Assert.assertEquals("test_task", task.getName());
        Assert.assertEquals(USER1.getId(), task.getUserId());
    }

    @Test
    public void createTaskNameDescription() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        @NotNull TaskDTO task = taskService.create(USER1.getId(), "test_task", "test_description");
        Assert.assertEquals(task.getId(), taskService.findOneById(task.getId()).getId());
        Assert.assertEquals("test_task", task.getName());
        Assert.assertEquals("test_description", task.getDescription());
        Assert.assertEquals(USER1.getId(), task.getUserId());
    }

    @Test
    public void updateById() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        @NotNull TaskDTO task = taskService.create(USER1.getId(), "test_task", "test_description");
        taskService.updateById(USER1.getId(), task.getId(), "new name", "new description");
        Assert.assertEquals("new name", taskService.findOneById(task.getId()).getName());
        Assert.assertEquals("new description", taskService.findOneById(task.getId()).getDescription());
    }

}
