package ru.t1.vlvov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO TM_USER " +
            "(ID, LOGIN, PASSWORD, FIRST_NAME, LAST_NAME, MIDDLE_NAME, EMAIL, ROLE, LOCKED)"
            + "VALUES (#{id}, #{login}, #{passwordHash}, #{firstName}, #{lastName}, #{middleName}, #{email}, #{role}, #{locked})")
    void addUser(@NotNull UserDTO user);

    @Delete("TRUNCATE TABLE TM_USER")
    void clearUser();

    @Select("SELECT * FROM TM_USER")
    @Results(value = {
            @Result(property = "passwordHash", column = "PASSWORD"),
            @Result(property = "firstName", column = "FIRST_NAME"),
            @Result(property = "lastName", column = "LAST_NAME"),
            @Result(property = "middleName", column = "MIDDLE_NAME")
    })
    @NotNull
    List<UserDTO> findAllUser();

    @Select("SELECT * FROM TM_USER WHERE ID = #{id}")
    @Results(value = {
            @Result(property = "passwordHash", column = "PASSWORD"),
            @Result(property = "firstName", column = "FIRST_NAME"),
            @Result(property = "lastName", column = "LAST_NAME"),
            @Result(property = "middleName", column = "MIDDLE_NAME")
    })
    @Nullable
    UserDTO findUserById(@Param("id") @NotNull String id);

    @Delete("DELETE FROM TM_USER WHERE ID = #{id}")
    void removeUser(@NotNull UserDTO user);

    @Delete("DELETE FROM TM_USER WHERE ID = #{id}")
    void removeUserById(@Param("id") @NotNull String id);

    @Update("UPDATE TM_USER SET LOGIN = #{login}, " +
            "PASSWORD = #{passwordHash}, " +
            "FIRST_NAME = #{firstName}, " +
            "LAST_NAME = #{lastName}, " +
            "MIDDLE_NAME = #{middleName}, " +
            "EMAIL = #{email}, " +
            "ROLE = #{role}, " +
            "LOCKED = #{locked} " +
            "WHERE ID = #{id}")
    void updateUser(@NotNull UserDTO user);

    @Select("SELECT * FROM TM_USER WHERE LOGIN = #{login}")
    @Results(value = {
            @Result(property = "passwordHash", column = "PASSWORD"),
            @Result(property = "firstName", column = "FIRST_NAME"),
            @Result(property = "lastName", column = "LAST_NAME"),
            @Result(property = "middleName", column = "MIDDLE_NAME")
    })
    @Nullable
    UserDTO findByLogin(@Param("login") @NotNull String login);

    @Select("SELECT * FROM TM_USER WHERE EMAIL = #{email}")
    @Results(value = {
            @Result(property = "passwordHash", column = "PASSWORD"),
            @Result(property = "firstName", column = "FIRST_NAME"),
            @Result(property = "lastName", column = "LAST_NAME"),
            @Result(property = "middleName", column = "MIDDLE_NAME")
    })
    @Nullable
    UserDTO findByEmail(@Param("email") @NotNull String email);

}
