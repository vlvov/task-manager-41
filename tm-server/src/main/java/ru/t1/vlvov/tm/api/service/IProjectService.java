package ru.t1.vlvov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.dto.model.ProjectDTO;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    @Nullable
    ProjectDTO changeProjectStatusById(@Nullable String userId,
                                       @Nullable String id,
                                       @NotNull Status status);


    @Nullable
    ProjectDTO create(@Nullable String userId, @Nullable String name);

    @Nullable
    ProjectDTO create(@Nullable String userId, @Nullable String name, @NotNull String description);

    @Nullable
    ProjectDTO updateById(@Nullable String userId,
                          @Nullable String id,
                          @Nullable String name,
                          @NotNull String description);

    @NotNull
    @SneakyThrows
    ProjectDTO add(@Nullable ProjectDTO model);

    @NotNull
    @SneakyThrows
    Collection<ProjectDTO> add(@Nullable Collection<ProjectDTO> projects);

    @NotNull
    @SneakyThrows
    Collection<ProjectDTO> set(@Nullable Collection<ProjectDTO> models);

    @SneakyThrows
    void clear();

    @NotNull
    @SneakyThrows
    List<ProjectDTO> findAll();

    @NotNull
    @SneakyThrows
    ProjectDTO remove(@Nullable ProjectDTO project);

    @Nullable
    @SneakyThrows
    ProjectDTO removeById(@Nullable String id);

    @Nullable
    @SneakyThrows
    ProjectDTO findOneById(@Nullable String id);

    @Nullable
    @SneakyThrows
    List<ProjectDTO> findAll(@Nullable Comparator comparator);

    @SneakyThrows
    boolean existsById(@Nullable String id);

    @NotNull
    @SneakyThrows
    ProjectDTO add(@Nullable String userId, @Nullable ProjectDTO project);

    @SneakyThrows
    void clear(@Nullable String userId);

    @Nullable
    @SneakyThrows
    List<ProjectDTO> findAll(@Nullable String userId);

    @Nullable
    @SneakyThrows
    ProjectDTO findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    @SneakyThrows
    ProjectDTO remove(@Nullable String userId, @Nullable ProjectDTO project);

    @Nullable
    @SneakyThrows
    ProjectDTO removeById(@Nullable String userId, @Nullable String id);

    @Nullable
    @SneakyThrows
    List<ProjectDTO> findAll(@Nullable String userId, @Nullable Comparator comparator);

    @SneakyThrows
    boolean existsById(@Nullable String userId, @Nullable String id);
}
