package ru.t1.vlvov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.dto.model.TaskDTO;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    @Nullable
    TaskDTO changeTaskStatusById(@Nullable String userId, @Nullable String id, @NotNull Status status);

    @Nullable
    TaskDTO create(@Nullable String userId, @Nullable String name);

    @Nullable
    TaskDTO create(@Nullable String userId, @Nullable String name, @NotNull String description);

    @NotNull
    @SneakyThrows
    TaskDTO add(@Nullable TaskDTO model);

    @NotNull
    @SneakyThrows
    Collection<TaskDTO> add(@Nullable Collection<TaskDTO> tasks);

    @NotNull
    @SneakyThrows
    Collection<TaskDTO> set(@Nullable Collection<TaskDTO> models);

    @SneakyThrows
    void clear();

    @NotNull
    @SneakyThrows
    List<TaskDTO> findAll();

    @NotNull
    @SneakyThrows
    TaskDTO remove(@Nullable TaskDTO task);

    @Nullable
    @SneakyThrows
    TaskDTO removeById(@Nullable String id);

    @Nullable
    @SneakyThrows
    TaskDTO findOneById(@Nullable String id);

    @Nullable
    @SneakyThrows
    List<TaskDTO> findAll(@Nullable Comparator comparator);

    @SneakyThrows
    boolean existsById(@Nullable String id);

    @NotNull
    @SneakyThrows
    TaskDTO add(@Nullable String userId, @Nullable TaskDTO task);

    @SneakyThrows
    void clear(@Nullable String userId);

    @Nullable
    @SneakyThrows
    List<TaskDTO> findAll(@Nullable String userId);

    @Nullable
    @SneakyThrows
    TaskDTO findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    @SneakyThrows
    TaskDTO remove(@Nullable String userId, @Nullable TaskDTO task);

    @Nullable
    @SneakyThrows
    TaskDTO removeById(@Nullable String userId, @Nullable String id);

    @Nullable
    @SneakyThrows
    List<TaskDTO> findAll(@Nullable String userId, @Nullable Comparator comparator);

    @SneakyThrows
    boolean existsById(@Nullable String userId, @Nullable String id);

    @Nullable
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @Nullable
    TaskDTO updateById(@Nullable String userId,
                       @Nullable String id,
                       @Nullable String name,
                       @NotNull String description);

    @Nullable
    @SneakyThrows
    List<TaskDTO> findAllByProjectId(@Nullable String projectId);
}
