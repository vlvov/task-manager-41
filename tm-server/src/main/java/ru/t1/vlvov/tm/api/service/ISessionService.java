package ru.t1.vlvov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.dto.model.SessionDTO;

import java.util.Collection;
import java.util.List;

public interface ISessionService {
    @NotNull
    @SneakyThrows
    SessionDTO add(@Nullable SessionDTO model);

    @NotNull
    @SneakyThrows
    Collection<SessionDTO> add(@Nullable Collection<SessionDTO> sessions);

    @NotNull
    @SneakyThrows
    Collection<SessionDTO> set(@Nullable Collection<SessionDTO> models);

    @SneakyThrows
    void clear();

    @NotNull
    @SneakyThrows
    List<SessionDTO> findAll();

    @NotNull
    @SneakyThrows
    SessionDTO remove(@Nullable SessionDTO session);

    @Nullable
    @SneakyThrows
    SessionDTO removeById(@Nullable String id);

    @Nullable
    @SneakyThrows
    SessionDTO findOneById(@Nullable String id);

    @SneakyThrows
    boolean existsById(@Nullable String id);

    @NotNull
    @SneakyThrows
    SessionDTO add(@Nullable String userId, @Nullable SessionDTO session);

    @SneakyThrows
    void clear(@Nullable String userId);

    @Nullable
    @SneakyThrows
    List<SessionDTO> findAll(@Nullable String userId);

    @Nullable
    @SneakyThrows
    SessionDTO findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    @SneakyThrows
    SessionDTO remove(@Nullable String userId, @Nullable SessionDTO session);

    @Nullable
    @SneakyThrows
    SessionDTO removeById(@Nullable String userId, @Nullable String id);

    @SneakyThrows
    boolean existsById(@Nullable String userId, @Nullable String id);
}
