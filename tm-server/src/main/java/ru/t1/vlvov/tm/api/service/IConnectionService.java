package ru.t1.vlvov.tm.api.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManagerFactory;

public interface IConnectionService {

    @NotNull
    @SneakyThrows
    SqlSession getSqlSession();

    @NotNull SqlSessionFactory getSqlSessionFactory();

    @NotNull EntityManagerFactory factory();
}
