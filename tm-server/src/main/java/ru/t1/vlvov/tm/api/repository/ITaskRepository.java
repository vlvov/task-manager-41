package ru.t1.vlvov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.dto.model.TaskDTO;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    @Insert("INSERT INTO TM_TASK (ID, CREATED, NAME, DESCRIPTION, STATUS, USER_ID, PROJECT_ID)"
            + "VALUES (#{id}, #{created}, #{name}, #{description}, #{status}, #{userId}, #{projectId})")
    void addTask(@NotNull TaskDTO task);

    @Delete("DELETE FROM TM_TASK WHERE USER_ID = #{userId}")
    void clearTaskUserOwned(@Param("userId") @NotNull String userId);

    @Delete("TRUNCATE TABLE TM_TASK")
    void clearTask();

    @Select("SELECT * FROM TM_TASK")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @NotNull
    List<TaskDTO> findAllTask();

    @Select("SELECT * FROM TM_TASK WHERE ID = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable
    TaskDTO findTaskById(@Param("id") @NotNull String id);

    @Delete("DELETE FROM TM_TASK WHERE ID = #{id}")
    void removeTask(@NotNull TaskDTO task);

    @Delete("DELETE FROM TM_TASK WHERE ID = #{id}")
    void removeTaskById(@Param("id") @NotNull String id);

    @Select("SELECT * FROM TM_TASK ORDER BY NAME")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable
    List<TaskDTO> findAllTaskSortByName(@NotNull Comparator comparator);

    @Select("SELECT * FROM TM_TASK WHERE USER_ID = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable
    List<TaskDTO> findAllTaskUserOwned(@Param("userId") @NotNull String userId);

    @Select("SELECT * FROM TM_TASK WHERE USER_ID = #{userId} AND ID = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable
    TaskDTO findTaskByIdUserOwned(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Delete("DELETE FROM TM_TASK WHERE ID = #{id} AND USER_ID = #{userId}")
    void removeTaskByIdUserOwned(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Select("SELECT * FROM TM_TASK WHERE USER_ID = #{userId} ORDER BY CREATED")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable
    List<TaskDTO> findAllTaskSortByCreated(@Param("userId") @NotNull String userId, @NotNull Comparator comparator);

    @Update("UPDATE TM_TASK SET NAME = #{name}, DESCRIPTION = #{description}, STATUS = #{status}, PROJECT_ID = #{projectId} WHERE ID = #{id}")
    void updateTask(@NotNull TaskDTO task);

    @Update("UPDATE TM_TASK SET NAME = #{name}, DESCRIPTION = #{description}, STATUS = #{status}, PROJECT_ID = #{projectId} WHERE ID = #{id} AND USER_ID = #{userId}")
    void updateTaskUserOwned(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Select("SELECT * FROM TM_TASK WHERE PROJECT_ID = #{projectId}")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable
    List<TaskDTO> findAllByProjectId(@Param("projectId") @NotNull String projectId);

    @Select("SELECT * FROM TM_TASK WHERE USER_ID = #{userId} AND PROJECT_ID = #{projectId}")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @NotNull
    List<TaskDTO> findAllByProjectIdUserOwned(@Param("userId") @NotNull String userId, @Param("projectId") String projectId);

}
